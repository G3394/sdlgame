#ifndef functions
#define functions

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>

void renderText(SDL_Renderer* renderer, std::string text, TTF_Font* theFont, SDL_Color color, int x, int y);

bool checkCollision(SDL_Rect a, SDL_Rect b);

SDL_Texture* loadTexture( std::string path, SDL_Renderer* renderer);

void drawCursor(SDL_Renderer* renderer, SDL_Rect cursor, SDL_Rect origin, SDL_Texture* cursorImage);

#endif
