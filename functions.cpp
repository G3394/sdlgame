#include"functions.h"

void renderText(SDL_Renderer* renderer, std::string text, TTF_Font* theFont, SDL_Color color, int x, int y)
{
  int w,h;
  SDL_Surface* surfaceMessage = TTF_RenderText_Blended(theFont, text.c_str(), color);
  SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
  SDL_QueryTexture(Message, NULL, NULL, &w, &h);
  SDL_Rect temp{x,y,w,h};
  SDL_RenderCopy(renderer, Message, NULL, &temp);

  SDL_FreeSurface(surfaceMessage);
  SDL_DestroyTexture(Message);
}

bool checkCollision(SDL_Rect a, SDL_Rect b)
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    //Calculate the sides of rect B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;
    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

SDL_Texture* loadTexture( std::string path, SDL_Renderer* renderer)
{
	//The final SDL_Texture
	SDL_Texture* newTexture = NULL;
	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", path.c_str(), IMG_GetError() );
	}
  else
	{
		//create texture from the surface
    SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 255, 0, 255));

		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		if(newTexture == NULL)
		{
			printf("unable to create texture from %s! Error: %s\n", path.c_str(), SDL_GetError());
		}
		//get rid of the old loaded surfaces
		SDL_FreeSurface(loadedSurface);
	}
	return newTexture;
}

void drawCursor(SDL_Renderer* renderer, SDL_Rect cursor, SDL_Rect origin, SDL_Texture* cursorImage)
{
  SDL_Rect temp;
  temp.x = cursor.x;
  temp.y = cursor.y;
  temp.w = 16;
  temp.h = 16;

  double tx = temp.x - origin.x;
  double ty = temp.y - origin.y;
  double pi = 3.14;
  double angle = atan2(ty, tx) * 180 / pi;

  if(angle != 0.0)
  {
    angle += 90.0;
  }

  SDL_RenderCopyEx(renderer, cursorImage, NULL, &temp, angle, NULL, SDL_FLIP_NONE );

}
