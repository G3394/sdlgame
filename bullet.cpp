#include"bullet.h"

void Bullet::init()
{
  rect.x = rand() % 500;
  rect.y = rand() % 500;
  rect.w = 20;
  rect.h = 20;

  xTemp = yTemp = 0;

  xVel = yVel = 0;

  active = false;
  angle = 0.0;
}

void Bullet::spawn(SDL_Rect start, SDL_Rect target)
{
  xTemp = start.x;
  yTemp = start.y;
  rect.x = xTemp;
  rect.y = yTemp;

  setVel(target);
  active = true;
}

void Bullet::doStuff(SDL_Renderer* renderer, SDL_Rect start, SDL_Texture* bulletImage, SDL_Rect level, bool paused)
{
  if(active)
  {
    if(!paused)
    {
      moveX();
      moveY();
    }
    SDL_RenderCopyEx(renderer, bulletImage, NULL, &rect, angle, NULL, SDL_FLIP_NONE );
  }
  else
  {
    xTemp = start.x;
    yTemp = start.y;
  }
  if(rect.x < level.x || rect.x > level.x + (level.w * 2 - rect.w) || rect.y < level.y || rect.y > level.y + (level.h * 2 - rect.h))
  {
    active = false;
  }
}

SDL_Rect Bullet::getRect()
{
  return rect;
}

bool Bullet::getActive()
{
  return active;
}

void Bullet::moveX()
{
  xTemp += xVel;
  rect.x = xTemp;

}

void Bullet::moveY()
{
  yTemp += yVel;
  rect.y = yTemp;
}

void Bullet::setVel(SDL_Rect target)
{
  double tx = target.x - rect.x;
  double ty = target.y - rect.y;

  double pi = 3.14;

  angle = atan2(ty, tx) * 180 / pi;

  xVel = 10*(cos(angle*pi/180));   // move x
  yVel = 10*(sin(angle*pi/180));   // move x
}

void Bullet::scrollX(int vel)
{
  xTemp -= vel;
  rect.x = xTemp;
}

void Bullet::scrollY(int vel)
{
  yTemp -= vel;
  rect.y = yTemp;
}
