#ifndef _Options_
#define _Options

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<stdio.h>
#include<string>
#include<stdlib.h>
#include<time.h>
#include <sstream>

#include"keyStates.h"
#include"functions.h"

class OptionMenu {
private:
  int status;
  SDL_Rect menuRect, option1, option1Checker, option2, option2Checker, optionQuit, fpsRect, cursor;
  SDL_Event eventHandler;
  int mousePosX, mousePosY;
  bool quitMenu;
  keyChecker keyEngine;
  bool gameKeys[4];
  SDL_Color black, green;

  std::stringstream fpsText;
public:
  void init(int cursorStyle, bool showFps);
  void doMenu(
    SDL_Renderer* renderer,
    SDL_Texture* gameBackGround,
    TTF_Font* gameFont,
    float avgFPS,
    bool showFps,
    SDL_Texture* cursor1,
    SDL_Texture* cursor2,
    SDL_Texture* cursorDuck,
    SDL_Texture* cursorWatcher,
    SDL_Texture* cursorCrossHair,
    int cursorStyle );
    int cursorSetter;
    bool fpsSetter;
  bool getMenuStatus();
  int getStatus();
  void setStatus(int status);
  void checkEnemey(SDL_Renderer* renderer);
  int getMouseX();
  int getMouseY();
  void setMouse(int x, int y);
  int getCursor();
  bool getFps();
};
#endif
