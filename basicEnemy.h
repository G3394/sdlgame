#ifndef _Basic_Enemy
#define _Basic_Enemy

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<stdio.h>
#include<string>
#include<stdlib.h>
#include <time.h>

#include"functions.h"
#include"particle.h"
class BasicEnemy {
private:
  SDL_Rect rect;
  int xVel, yVel, positionCounter, positionX, positionY, respawnCounter;
  SDL_Rect level;
  Particle particles[30];
  bool active;
public:
  void init(SDL_Rect quad1);
  void spawn();
  void die();
  bool getActive();
  void doStuff(SDL_Renderer* renderer, SDL_Rect target, SDL_Texture* garyImage, SDL_Texture* garyGlowImage, SDL_Rect quad1, bool paused);
  SDL_Rect getRect();
  void moveX();
  void moveY();

  void moveBackX();
  void moveBackY();

  void scrollX(int vel);
  void scrollY(int vel);
};
#endif
