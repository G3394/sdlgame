#ifndef keyStates
#define keyStates

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>

class keyChecker {
  private:
    bool keys[5];
  public:
    void setKeys();
    void updateKeys();
    bool getKeys(int index);
};
#endif
