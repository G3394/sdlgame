#ifndef _Classic_
#define _Classic_

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<stdio.h>
#include<string>
#include<stdlib.h>
#include<time.h>
#include<sstream>

#include"keyStates.h"
#include"functions.h"
#include"basicEnemy.h"
#include"player.h"
#include"bullet.h"

class ClassicMode {
private:
  int status, SCREEN_WIDTH, SCREEN_HEIGHT, levelWidth, levelHei;
  int points, multiplier, multiplierUpgrade, enemyCount;
  SDL_Rect optionQuit, cursor, fpsRect;
  SDL_Rect hud1, hud2;
  SDL_Event eventHandler;
  int mousePosX, mousePosY;
  bool quitClassic;
  keyChecker keyEngine;
  bool gameKeys[5];
  bool paused, pauseSet;
  SDL_Color black, white, green;
  BasicEnemy basicEnemy[20];
  Player player;
  bool shoot;
  int tick, counter;
  Bullet bullets[100];
  //the four sections of the level
  SDL_Rect quad1, quad2, quad3, quad4, border;


  std::stringstream fpsText;
  std::stringstream pointsText;
  std::stringstream multiplierText;
public:
  void init(int height, int width);
  void doClassic(
    SDL_Renderer* renderer,
    SDL_Texture* gameBackGround,
    TTF_Font* gameFont,
    float avgFPS,
    bool showFps,
    SDL_Texture* cursor1,
    SDL_Texture* cursor2,
    SDL_Texture* cursorDuck,
    SDL_Texture* cursorWatcher,
    SDL_Texture* cursorCrossHair,
    SDL_Texture* playerImage,
    SDL_Texture* garyImage,
    SDL_Texture* garyGlowImage,
    SDL_Texture* bulletImage,
    int cursorStyle,
    SDL_Window* window );

  bool getClassicStatus();
  int getStatus();
  void setStatus(int status);
  void checkEnemey(SDL_Renderer* renderer, SDL_Texture* garyImage, SDL_Texture* garyGlowImage);
  void checkMap(SDL_Window* window);
  int getMouseX();
  int getMouseY();
  void setMouse(int x, int y);

  void setMouse(SDL_Window* window);
};

#endif
