#ifndef _Particle_Class_
#define _Particle_Class_

//this is basically the same as bullet .h but has a few different variables.
//will be used to simulate an explosion.

//possible to combie this and bullet .h at a later time.
//use default values for the difference in variables.
//then if the variable is == NULL make neccasary changes.
//such as width/height and the image/shape being drawn.

//for example: bullets currently do not accept a speed but particles do
//so bullets could have could accept speed with a default of NULL or 0
//and if they are spawned with that change that to whatever the speed is now.
//but if its a particle, give it the needed speed and then have it not change the speed in the spawn function.

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<stdio.h>
#include<string>
#include<stdlib.h>
#include <time.h>

#include"functions.h"

class Particle {
private:
  SDL_Rect rect;
  double xVel, yVel;
  double xTemp, yTemp;
  double angle;
  int lifeSpan;
  bool active;
public:
  void init();
  void spawn(SDL_Rect start, SDL_Rect target, int speed);
  void doStuff(SDL_Renderer* renderer, SDL_Rect target, bool paused);
  bool getActive();
  SDL_Rect getRect();
  void moveX();
  void moveY();
  void setVel(SDL_Rect target, int speed);
  void scrollX(int vel);
  void scrollY(int vel);
};
#endif
