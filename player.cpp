#include"player.h"

void Player::init(int SCREEN_WIDTH, int SCREEN_HEIGHT)
{
  rect.x = SCREEN_WIDTH / 2;
  rect.y = SCREEN_HEIGHT / 2;
  rect.w = 20;
  rect.h = 20;

  xVel = yVel = 0;
  angle = 0;
}

void Player::doStuff(SDL_Renderer* renderer, SDL_Texture* playerImage, bool paused)
{
  if(!paused)
  {
    rect.x += xVel;
    rect.y += yVel;
  }
  if(xVel < 0 && yVel == 0)
  {
    angle = 270;
  }
  else if(xVel < 0 && yVel < 0)
  {
    angle = 315;
  }
  else if(xVel == 0 && yVel < 0)
  {
    angle = 0;
  }
  else if(xVel > 0 && yVel < 0)
  {
    angle = 45;
  }
  else if(xVel > 0 && yVel == 0)
  {
    angle = 90;
  }
  else if(xVel > 0 && yVel > 0)
  {
    angle = 135;
  }
  else if(xVel == 0 && yVel > 0)
  {
    angle = 180;
  }
  else if(xVel < 0 && yVel > 0)
  {
    angle = 225;
  }
  SDL_RenderCopyEx(renderer, playerImage, NULL, &rect, angle, NULL, SDL_FLIP_NONE );
}

SDL_Rect Player::getRect()
{
  return rect;
}

int Player::getXvel()
{
  return xVel;
}

int Player::getYvel()
{
  return yVel;
}

void Player::setXvel(int vel)
{
  xVel = vel;
}

void Player::setYvel(int vel)
{
  yVel = vel;
}

void Player:: moveBackX()
{
  rect.x -= xVel;
}
void Player::moveBackY()
{
  rect.y -= yVel;
}
