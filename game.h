//One game object to rule them all.
//this and the corresponding cpp file will handle most/all game logic.
#ifndef game
#define game

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>

#include"keyStates.h"
#include"menu.h"
#include"classic.h"
#include"options.h"
#include"functions.h"
#include"timer.h"

class Game {
  private:
    int status;
    SDL_Window* window;
    //the renderer
    SDL_Renderer* renderer;
    //images
    SDL_Texture* gameBackGround;
    SDL_Texture* playerImage;
    SDL_Texture* cursor1;
    SDL_Texture* cursor2;
    SDL_Texture* cursorDuck;
    SDL_Texture* cursorWatcher;
    SDL_Texture* cursorCrossHair;
    SDL_Texture* garyImage;
    SDL_Texture* garyGlowImage;
    SDL_Texture* bulletImage;
    //end images
    bool quitGame;
    //Screen dimension constants
    int SCREEN_WIDTH;
    int SCREEN_HEIGHT;
    //the TTF_Font
    TTF_Font* gameFont;

    Menu gameMenu;
    ClassicMode classicMode;
    OptionMenu optionMenu;

    int countedFrames;
    Timer fpsTimer;
    int cursorStyle;
    bool showFps;
  public:
    void init();
    void gameLoad();
    void doGame();
    void quit();
};

#endif
