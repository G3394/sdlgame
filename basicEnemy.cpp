#include"basicEnemy.h"

void BasicEnemy::init(SDL_Rect quad1)
{
  level.x = quad1.x;
  level.y = quad1.y;
  level.w = quad1.w * 2;
  level.h = quad1.h * 2;

  rect.x = rand() % (level.w - rect.w) + level.x;
  rect.y = rand() % (level.h - rect.h) + level.y;

  rect.w = 20;
  rect.h = 20;

  xVel = yVel = 0;
  respawnCounter = 0;

  active = true;

  positionCounter = 0;
  positionX = rect.x;
  positionY = rect.y;

  for(int i = 0; i < 30; i++)
  {
    particles[i].init();
  }
}

void BasicEnemy::spawn()
{
  rect.x = rand() % (level.w - rect.w) + level.x;
  rect.y = rand() % (level.h - rect.h) + level.y;
  rect.w = 20;
  rect.h = 20;

  xVel = yVel = 0;

  positionCounter = 0;
  positionX = rect.x;
  positionY = rect.y;

  active = true;
}

void BasicEnemy::die()
{
  respawnCounter = 0;
  int tempSpeed = 0;
  SDL_Rect tempTarget;
  tempTarget = {rect.x, rect.y, 1, 1};

  for(int i = 0; i < 30; i++)
  {
    tempSpeed = rand() % 2 + 1;
    tempTarget.x = rand() % 20 + (rect.x - 10);
    tempTarget.y = rand() %  20 + (rect.y - 10);

    particles[i].spawn(rect, tempTarget, tempSpeed);
  }

  active = false;
}

bool BasicEnemy::getActive()
{
  return active;
}

void BasicEnemy::doStuff(SDL_Renderer* renderer, SDL_Rect target, SDL_Texture* garyImage, SDL_Texture* garyGlowImage, SDL_Rect quad1, bool paused)
{//enemy moving functions take place in the object that owns them so that it can check collisions with other copies.
  level.x = quad1.x;
  level.y = quad1.y;
  if(active)
  {
    //find and set angle.
    double tx = target.x - rect.x;
    double ty = target.y - rect.y;

    double pi = 3.14;

    double angle = atan2(ty, tx) * 180 / pi;

    xVel = 2*(cos(angle*pi/180));   // move x
    yVel = 2*(sin(angle*pi/180));   // move x

    //check if the enemy has gotten stuck..

    if(!paused)
    {
      positionCounter++;
    }
    if(positionCounter > 1 && rect.x == positionX && rect.y == positionY)
    {
      spawn();
    }
    //if not reset the counter and positions to check if the player gets stuck after spawning.
    else if(positionCounter > 1)
    {
      positionCounter = 0;
      positionX = rect.x;
      positionY = rect.y;
    }
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 50);
    SDL_Rect temp; //clean this. velocity is a double but rects are only int. conversion screwy when moving
    temp.x = rect.x - 10;
    temp.y = rect.y - 10;
    temp.w = 40;
    temp.h = 40;
    SDL_SetTextureAlphaMod(garyGlowImage, 40);
    SDL_RenderCopyEx(renderer, garyGlowImage, NULL, &temp, angle, NULL, SDL_FLIP_NONE );
    SDL_RenderCopyEx(renderer, garyImage, NULL, &rect, angle, NULL, SDL_FLIP_NONE );
  }
  else
  {
    for(int i = 0; i < 30; i++)
    {
      particles[i].doStuff(renderer, rect, paused);
    }
    if(!paused)
    {
      respawnCounter++;
    }
    if(respawnCounter > 20)
    {
      spawn();
    }
  }
}

SDL_Rect BasicEnemy::getRect()
{
  return rect;
}

void BasicEnemy::moveX()
{
  rect.x += xVel;
}

void BasicEnemy::moveY()
{
  rect.y += yVel;
}

void BasicEnemy::moveBackX()
{
  rect.x -= xVel;
}

void BasicEnemy::moveBackY()
{
  rect.y -= yVel;
}

void BasicEnemy::scrollX(int vel)
{
  rect.x -= vel;
  for(int i = 0; i < 30; i++)
  {
    particles[i].scrollX(vel);
  }
}

void BasicEnemy::scrollY(int vel)
{
  rect.y -= vel;
  for(int i = 0; i < 30; i++)
  {
    particles[i].scrollY(vel);
  }
}
