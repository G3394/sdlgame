#include"menu.h"

void Menu::init()
{
  status = 1;
  menuItem = 1; //1 = play game 2 = options 3 = quit.

  menuRect.x = 400;
  menuRect.y = 300;
  menuRect.w = 200;
  menuRect.h = 100;

  option1.x = 470;
  option1.y = 320;
  option1.w = 70;
  option1.h = 20;

  option2.x = 470;
  option2.y = 345;
  option2.w = 50;
  option2.h = 20;

  optionQuit.x = 470;
  optionQuit.y = 365;
  optionQuit.w = 30;
  optionQuit.h = 20;

  fpsRect.x = 700;
  fpsRect.y = 30;
  fpsRect.w = 0;
  fpsRect.h = 0;

  cursor.x = 0;
  cursor.y = 0;
  cursor.w = 10;
  cursor.h = 10;

  mousePosX = mousePosY = 0;

  quitMenu = false;
  keyEngine.setKeys();

  black = {0, 0, 0};
  green = {30, 255, 0};
}

void Menu::doMenu(
  SDL_Renderer* renderer,
  SDL_Texture* gameBackGround,
  TTF_Font* gameFont,
  float avgFPS,
  bool showFps,
  SDL_Texture* cursor1,
  SDL_Texture* cursor2,
  SDL_Texture* cursorDuck,
  SDL_Texture* cursorWatcher,
  SDL_Texture* cursorCrossHair,
  int cursorStyle )
{
  //Mouse events
  while(SDL_PollEvent(&eventHandler) != 0)
  {
    if(eventHandler.type == SDL_QUIT)
    {
      quitMenu = true;
    }
    //mouse events
    if(eventHandler.type == SDL_MOUSEMOTION || eventHandler.type == SDL_MOUSEBUTTONDOWN || eventHandler.type == SDL_MOUSEBUTTONUP )
    {//update the mouse position
      SDL_GetMouseState(&mousePosX, &mousePosY );

      //update menuItem only when cursor is moved.
      if(checkCollision(cursor, option1))
      {
        menuItem = 1;
      }
      else if(checkCollision(cursor, option2))
      {
        menuItem = 2;
      }
      else if(checkCollision(cursor, optionQuit))
      {
        menuItem = 3;
      }
      //check if buttons were pressed or released
      switch (eventHandler.type) {
        case SDL_MOUSEBUTTONUP:
          if(eventHandler.button.button == SDL_BUTTON_LEFT){
            printf("left button released\n");
            if(checkCollision(cursor, option1))
            {
              status = 2;
            }
            else if(checkCollision(cursor, option2))
            {
              status = 3;
            }
            else if(checkCollision(cursor,optionQuit)){
              printf("You have quit the game. Goodbye");
              quitMenu = true;
            }
          }
          else if(eventHandler.button.button == SDL_BUTTON_RIGHT){
            printf("Right button released\n");
          }
          break;
        case SDL_MOUSEBUTTONDOWN:
          if(eventHandler.button.button == SDL_BUTTON_LEFT){
            printf("Left button pressed\n");
          }
          else if(eventHandler.button.button == SDL_BUTTON_RIGHT){
            printf("Right button pressed\n");
          }
          break;
      }
      printf("Mouse x pos:");
      printf("%d\n",mousePosX);
      printf("Mouse y pos:");
      printf("%d\n",mousePosY);
    }
  }
  //update the cursor position
  cursor.x = mousePosX;
  cursor.y = mousePosY;
  //update the keys and assign them to local variables.
  keyEngine.updateKeys();
  for(int i = 0; i < 5; i++)
  {
    gameKeys[i] = keyEngine.getKeys(i);
  }
  //set the draw color and clear the screen
  SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
  SDL_RenderClear(renderer);

  SDL_RenderCopy(renderer, gameBackGround, NULL, NULL);

  //draw a menu box
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 1);
  SDL_RenderFillRect(renderer, &menuRect);
  SDL_SetRenderDrawColor(renderer, 30, 255, 0, 1);
  SDL_RenderDrawRect(renderer, &menuRect);

  //draw the options to the Screen
  renderText(renderer, "Play Game", gameFont, green, option1.x, option1.y);
  renderText(renderer, "Options", gameFont, green, option2.x, option2.y);
  renderText(renderer, "quit", gameFont, green, optionQuit.x, optionQuit.y);

  SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
  switch (menuItem) {
    case 1:
      SDL_RenderDrawLine(renderer, option1.x, option1.y + option1.h, option1.x + option1.w, option1.y + option1.h);
      break;
    case 2:
      SDL_RenderDrawLine(renderer, option2.x, option2.y + option2.h, option2.x + option2.w, option2.y + option2.h);
      break;
    case 3:
      SDL_RenderDrawLine(renderer, optionQuit.x, optionQuit.y + optionQuit.h, optionQuit.x + optionQuit.w, optionQuit.y + optionQuit.h);
      break;

  }

  if(showFps)
  {
    fpsText.str( "" );
    fpsText << "Average Frames Per Second " << avgFPS;
    renderText(renderer, fpsText.str().c_str(), gameFont, green, fpsRect.x, fpsRect.y);
  }

  //draw the cursor
  switch (cursorStyle) {
    case 1:
      drawCursor(renderer, cursor, cursor, cursor1); //supply cursor as origin to stop rotation.
      break;
    case 2:
      drawCursor(renderer, cursor, option1, cursor2); //supply player rect to rotate cusor
      break;
    case 3:
      drawCursor(renderer, cursor, cursor, cursorDuck);
      break;
    case 4:
      drawCursor(renderer, cursor, option1, cursorWatcher);
      break;
    case 5:
      drawCursor(renderer, cursor, cursor, cursorCrossHair);
      break;
  }
  //update the Screen
  SDL_RenderPresent(renderer);
}

bool Menu::getMenuStatus()
{
  return quitMenu;
}

int Menu::getStatus()
{
  return status;
}

void Menu::setStatus(int status)
{
  this->status = status;
}

int Menu::getMouseX()
{
  return mousePosX;
}

int Menu::getMouseY()
{
  return mousePosY;
}

void Menu::setMouse(int x, int y)
{
  mousePosX = x;
  mousePosY = y;
}
