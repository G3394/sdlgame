//this file will serve as a controller for the game.
//it will handle initializeing stuff as well as deciding what what happens.
//do menu, do game mode, do options, etc.

#include"game.h"

void Game::init()
{
  status = 1;
  //The window we'll be rendering to
  window = NULL;

  gameBackGround = NULL;

  SCREEN_WIDTH = 0;
  SCREEN_HEIGHT = 0;

  quitGame = false;

  gameFont = NULL;

  //Initialize SDL
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
  {
      printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
  }
  else
  {
    SDL_DisplayMode dm;

    if (SDL_GetDesktopDisplayMode(0, &dm) != 0)
    {
        printf("Could not get display mode for video display %s", SDL_GetError());
    }

    SCREEN_WIDTH = dm.w;
    SCREEN_HEIGHT = dm.h;

  //Create window
  window = SDL_CreateWindow( "SDL Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
  if( window == NULL )
    {
      printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
    }
  else
    {
      //create the renderer
      renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
      SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);
      if(renderer == NULL)
      {
        printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
      }
      else
      {
        //init renderer color
        SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xff, 0xff);
        //init png loading
        int imgFlags = IMG_INIT_PNG;
        if(!(IMG_Init(imgFlags) & imgFlags))
        {
          printf("SDL_image could not init! SDL)image Error: %s\n", IMG_GetError());
        }
        //init ttf
        if(TTF_Init() == -1)
        {
          printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
        }
        else
        {
          gameFont = TTF_OpenFont("Roboto-Regular.ttf", 13);
          if(gameFont == NULL)
          {
            printf( "Failed to load FontFile! SDL_ttf Error: %s\n", TTF_GetError() );
          }
        }
      }
    }
  }

  //menu init
  gameMenu.init();
  classicMode.init(SCREEN_HEIGHT, SCREEN_WIDTH);
  cursorStyle = 5;
  showFps = false;
  optionMenu.init(cursorStyle, showFps);

  countedFrames = 0;
  fpsTimer.start();
}
///end init section

void Game::gameLoad()
{//very simple game loading. See load.h for more details.
  gameBackGround = loadTexture("images/Grid3.png", renderer);
  playerImage = loadTexture("images/player.png", renderer);
  cursor1 = loadTexture("images/cursor1.png", renderer);
  cursor2 = loadTexture("images/cursor2.png", renderer);
  cursorDuck = loadTexture("images/cursor-duck.png", renderer);
  cursorWatcher = loadTexture("images/cursor-watcher.png", renderer);
  cursorCrossHair = loadTexture("images/cursor-crosshair.png", renderer);
  garyImage = loadTexture("images/gary.png", renderer);
  garyGlowImage = loadTexture("images/gary-glow.png", renderer);
  bulletImage = loadTexture("images/bullet.png", renderer);
}

void Game::doGame()
{//here is where most game logic will be.
  while(!quitGame)
  {
    float avgFPS = countedFrames / (fpsTimer.getTicks() / 1000.f);
    if(avgFPS > 2000000)
    {
      avgFPS = 0;
    }
    //TO HIDE THE CURSOR AND DISPLAY SOMETHING COOL INSTEAD
    SDL_ShowCursor(SDL_DISABLE);

    //make sure all status for each object is accurate.
    gameMenu.setStatus(status);
    classicMode.setStatus(status);
    optionMenu.setStatus(status);
    //do the menu stuff
    switch (status) {
      case 1:
        gameMenu.doMenu(renderer,
        gameBackGround,
        gameFont,
        avgFPS,
        showFps,
        cursor1,
        cursor2,
        cursorDuck,
        cursorWatcher,
        cursorCrossHair,
        cursorStyle );

        //update classicMode mouse to match menu mouse
        classicMode.setMouse(gameMenu.getMouseX(), gameMenu.getMouseY());
        optionMenu.setMouse(gameMenu.getMouseX(), gameMenu.getMouseY());

        status = gameMenu.getStatus();
        break;

      case 2:
        classicMode.doClassic(renderer,
        gameBackGround,
        gameFont,
        avgFPS,
        showFps,
        cursor1,
        cursor2,
        cursorDuck,
        cursorWatcher,
        cursorCrossHair,
        playerImage,
        garyImage,
        garyGlowImage,
        bulletImage,
        cursorStyle,
        window );

        //update menu mouse to match classicMode mouse
        gameMenu.setMouse(classicMode.getMouseX(), classicMode.getMouseY());
        optionMenu.setMouse(classicMode.getMouseX(), classicMode.getMouseY());

        status = classicMode.getStatus();
        break;

      case 3:
        optionMenu.doMenu(renderer,
        gameBackGround,
        gameFont,
        avgFPS,
        showFps,
        cursor1,
        cursor2,
        cursorDuck,
        cursorWatcher,
        cursorCrossHair,
        cursorStyle );

        //update mouse for other modes
        gameMenu.setMouse(optionMenu.getMouseX(), optionMenu.getMouseY());
        classicMode.setMouse(optionMenu.getMouseX(), optionMenu.getMouseY());

        status = optionMenu.getStatus();
        //get settings that may have changed.
        cursorStyle = optionMenu.getCursor();
        showFps = optionMenu.getFps();
        break;
    }
    countedFrames++;

    if(gameMenu.getMenuStatus() || classicMode.getClassicStatus() || optionMenu.getMenuStatus())
    {
      quitGame = true;
    }
  }
}//end game logic section.

void Game::quit()
{
  //Deallocate surfaces
  SDL_DestroyTexture(gameBackGround);
  gameBackGround = NULL;
  SDL_DestroyTexture (playerImage);
  playerImage = NULL;
  SDL_DestroyTexture(cursor1);
  cursor1 = NULL;
  SDL_DestroyTexture(cursor2);
  cursor2 = NULL;
  SDL_DestroyTexture(cursorDuck);
  cursorDuck = NULL;
  SDL_DestroyTexture(cursorWatcher);
  cursorWatcher = NULL;
  SDL_DestroyTexture(cursorCrossHair);
  cursorCrossHair = NULL;
  SDL_DestroyTexture(garyImage);
  garyImage = NULL;
  SDL_DestroyTexture(garyGlowImage);
  garyGlowImage = NULL;
  SDL_DestroyTexture(bulletImage);
  bulletImage = NULL;
  //destroy the window
  SDL_DestroyWindow( window );
  window = NULL;
  //destory the SDL_Renderer
  SDL_DestroyRenderer(renderer);
  renderer = NULL;

  //Quit SDL subsystems
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}
