#include"classic.h"

void ClassicMode::init(int height, int width)
{
  SCREEN_HEIGHT = height;
  SCREEN_WIDTH = width;

  double tempW = width;
  double tempH = height;

  tempW = tempW * 0.75;
  tempH = tempH * 0.75;

  srand (time(NULL));

  status = 2;

  points = 0;
  multiplier = 1;
  multiplierUpgrade = 5;
  enemyCount = 0;

  optionQuit.x = 20;
  optionQuit.y = 65;
  optionQuit.w = 40;
  optionQuit.h = 90;

  cursor.x = 0;
  cursor.y = 0;
  cursor.w = 10;
  cursor.h = 10;

  fpsRect.x = 700;
  fpsRect.y = 30;
  fpsRect.w = 0;
  fpsRect.h = 0;

  mousePosX = mousePosY = 0;

  quitClassic = false;
  keyEngine.setKeys();
  paused = false;
  pauseSet = false;

  black = {0,0,0};
  white = {255,255,255};
  green = {30, 255, 0};

  player.init(SCREEN_WIDTH, SCREEN_HEIGHT);

  shoot = false;
  tick = 0;
  counter = 0;
  for(int i = 0; i < 100; i++)
  {
    bullets[i].init();
  }

  quad1.x = 0 - tempW / 2;
  quad1.y = 0 - tempH / 2;
  quad1.w = tempW;
  quad1.h = tempH;

  quad2.x = quad1.x + quad1.w;
  quad2.y = 0;
  quad2.w = tempW;
  quad2.h = tempH;

  quad3.x = 0;
  quad3.y = quad1.y + quad1.h;
  quad3.w = tempW;
  quad3.h = tempH;

  quad4.x = quad1.x + quad1.w;
  quad4.y = quad1.y + quad1.h;
  quad4.w = tempW;
  quad4.h = tempH;

  border.x = quad1.x;
  border.y = quad1.y;
  border.w = quad1.w * 2;
  border.h = quad1.h * 2;

  hud1.x = 20;
  hud1.y = 20;
  hud1.w = 120;
  hud1.h = 60;

  hud2.x = width - 140;
  hud2.y = 20;
  hud2.w = 120;
  hud2.h = 60;

  for(int i = 0; i < 20; i++)
  {
    basicEnemy[i].init(quad1);
  }

}

void ClassicMode::doClassic(
  SDL_Renderer* renderer,
  SDL_Texture* gameBackGround,
  TTF_Font* gameFont,
  float avgFPS,
  bool showFps,
  SDL_Texture* cursor1,
  SDL_Texture* cursor2,
  SDL_Texture* cursorDuck,
  SDL_Texture* cursorWatcher,
  SDL_Texture* cursorCrossHair,
  SDL_Texture* playerImage,
  SDL_Texture* garyImage,
  SDL_Texture* garyGlowImage,
  SDL_Texture* bulletImage,
  int cursorStyle,
  SDL_Window* window )
{
  //Mouse events
  while(SDL_PollEvent(&eventHandler) != 0)
  {
    if(eventHandler.type == SDL_QUIT)
    {
      quitClassic = true;
    }
    //mouse events
    if(eventHandler.type == SDL_MOUSEMOTION || eventHandler.type == SDL_MOUSEBUTTONDOWN || eventHandler.type == SDL_MOUSEBUTTONUP )
    {//update the mouse position
      SDL_GetMouseState(&mousePosX, &mousePosY );
      cursor.x = mousePosX;
      cursor.y = mousePosY;
      //check if buttons were pressed or released
      switch (eventHandler.type) {
        case SDL_MOUSEBUTTONUP:
          if(eventHandler.button.button == SDL_BUTTON_LEFT){
            printf("left button released\n");
            shoot = false;
            tick = 10;
            if(checkCollision(cursor,optionQuit)){
              status = 1;
            }
          }
          else if(eventHandler.button.button == SDL_BUTTON_RIGHT){
            printf("Right button released\n");
          }
          break;
        case SDL_MOUSEBUTTONDOWN:
          if(eventHandler.button.button == SDL_BUTTON_LEFT){
            printf("Left button pressed\n");
            shoot = true;
          }
          else if(eventHandler.button.button == SDL_BUTTON_RIGHT){
            printf("Right button pressed\n");
          }
          break;
      }
    }
  }
  //update the keys and assign them to local variables.
  keyEngine.updateKeys();
  for(int i = 0; i < 5; i++)
  {
    gameKeys[i] = keyEngine.getKeys(i);
  }

  if(gameKeys[0] && !gameKeys[2])
  {
    player.setYvel(-3);
  }
  else if(!gameKeys[0] && !gameKeys[2])
  {
    player.setYvel(0);
  }

  if(gameKeys[1] && !gameKeys[3])
  {
    player.setXvel(-3);
  }
  else if(!gameKeys[1] && !gameKeys[3])
  {
    player.setXvel(0);
  }

  if(gameKeys[2] && !gameKeys[0])
  {
    player.setYvel(3);
  }
  else if(!gameKeys[2] && !gameKeys[0])
  {
    player.setYvel(0);
  }

  if(gameKeys[3] && !gameKeys[1])
  {
    player.setXvel(3);
  }
  else if(!gameKeys[3] && !gameKeys[1])
  {
    player.setXvel(0);
  }

  //pause the game
  if(gameKeys[4])
  {
    if(!pauseSet && !paused)
    {
      paused = true;
      pauseSet = true;
    }
    else if(!pauseSet && paused)
    {
      paused = false;
      pauseSet = true;
    }
  }
  else if(!gameKeys[4])
  {
    pauseSet = false;
  }

  if(shoot && !paused)
  {
    tick++;
    if(tick > 10)
    {
      bullets[counter].spawn(player.getRect(), cursor);
      counter++;
      if(counter == 100)
      {
        counter = 0;
      }
      tick = 0;
    }
  }

  //set the draw color and clear the screen
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
  SDL_RenderClear(renderer);
  //render the background
  SDL_RenderCopyEx(renderer, gameBackGround, NULL, &quad1, 0.0, NULL, SDL_FLIP_NONE );
  SDL_RenderCopyEx(renderer, gameBackGround, NULL, &quad2, 0.0, NULL, SDL_FLIP_NONE );
  SDL_RenderCopyEx(renderer, gameBackGround, NULL, &quad3, 0.0, NULL, SDL_FLIP_NONE );
  SDL_RenderCopyEx(renderer, gameBackGround, NULL, &quad4, 0.0, NULL, SDL_FLIP_NONE );

  //draw a rect around the map.
  border.x = quad1.x;
  border.y = quad1.y;
  SDL_SetRenderDrawColor(renderer, 30, 255, 0, 0);
  SDL_RenderDrawRect(renderer, &border);
  //draw the hud
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 100);
  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
  SDL_RenderFillRect(renderer, &hud1);
  SDL_RenderFillRect(renderer, &hud2);
  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
  SDL_SetRenderDrawColor(renderer, 30, 255, 0, 0);
  SDL_RenderDrawRect(renderer, &hud1);
  SDL_RenderDrawRect(renderer, &hud2);
  //hud text.
  pointsText.str( "" );
  pointsText << "" << points;
  renderText(renderer, pointsText.str().c_str(), gameFont, white, hud1.x + 5, hud1.y + 5);
  //same
  multiplierText.str("");
  multiplierText << "" << multiplier;
  renderText(renderer, multiplierText.str().c_str(), gameFont, white, hud1.x + 5, hud1.y + 20);

  //check basic enemies for collision with eachother then move them.
  checkEnemey(renderer, garyImage, garyGlowImage);

  //do player and cursor stuff
  player.doStuff(renderer, playerImage, paused);
  setMouse(window);
  cursor.x = mousePosX;
  cursor.y = mousePosY;

  //SDL_WarpMouseInWindow(window, mousePosX, mousePosY);

  //do bullet things
  for(int i = 0; i < 100; i++)
  {
    bullets[i].doStuff(renderer, player.getRect(), bulletImage, quad1, paused);
  }
  if(paused)
  {
    printf("paused");
  }

  //check the map compared to player. scroll things.
  checkMap(window);

  //Render cursor
  switch (cursorStyle) {
    case 1:
      drawCursor(renderer, cursor, cursor, cursor1); //supply cursor as origin to stop rotation.
      break;
    case 2:
      drawCursor(renderer, cursor, player.getRect(), cursor2); //supply player rect to rotate cusor
      break;
    case 3:
      drawCursor(renderer, cursor, cursor, cursorDuck);
      break;
    case 4:
      drawCursor(renderer, cursor, player.getRect(), cursorWatcher);
      break;
    case 5:
      drawCursor(renderer, cursor, cursor, cursorCrossHair);
      break;
  }

  //draw the options to the Screen
  renderText(renderer, "return to menu", gameFont, white, optionQuit.x, optionQuit.y);

  //obvi
  if(showFps)
  {
    fpsText.str( "" );
    fpsText << "Average Frames Per Second " << avgFPS;
    renderText(renderer, fpsText.str().c_str(), gameFont, green, fpsRect.x, fpsRect.y);
  }

  //update the Screen
  SDL_RenderPresent(renderer);
}

bool ClassicMode::getClassicStatus()
{
  return quitClassic;
}

int ClassicMode::getStatus()
{
  return status;
}
void ClassicMode::setStatus(int status)
{
  this->status = status;
}

void ClassicMode::checkEnemey(SDL_Renderer* renderer, SDL_Texture* garyImage, SDL_Texture* garyGlowImage)
{
  for(int i = 0; i < 20; i++)
  {
    bool hitX = false;
    bool hitY = false;

    if(!paused)
    {
      basicEnemy[i].moveX();
    }
    for(int x = 0; x < 20; x++)
    {
      if(i != x)
      {
        if(checkCollision(basicEnemy[i].getRect(), basicEnemy[x].getRect()))
        {
          hitX = true;
        }
      }
    }
    if(hitX)
    {
      basicEnemy[i].moveBackX();
    }

    if(!paused)
    {
      basicEnemy[i].moveY();
    }
    for(int x = 0; x < 20; x++)
    {
      if(i != x)
      {
        if(checkCollision(basicEnemy[i].getRect(), basicEnemy[x].getRect()))
        {
          hitY = true;
        }
      }
    }
    if(hitY)
    {
      basicEnemy[i].moveBackY();
    }
    basicEnemy[i].doStuff(renderer, player.getRect(), garyImage, garyGlowImage, quad1, paused);

    for(int b = 0; b < 100; b++)
    {
      if(bullets[b].getActive())
      {
        if(checkCollision(basicEnemy[i].getRect(), bullets[b].getRect()) && basicEnemy[i].getActive() )
        {
          basicEnemy[i].die();
          points += 5 * multiplier;
          enemyCount++;
          if(enemyCount >= multiplierUpgrade)
          {
            multiplierUpgrade += multiplierUpgrade;
            multiplier++;
          }
        }
      }
    }
  }
}

void ClassicMode::checkMap(SDL_Window* window)
{
  int xVel = player.getXvel();
  int yVel = player.getYvel();

  //if player is close to edge of screen
  if(player.getRect().x < 200 && quad1.x < 50)
  {
    //move player and map opposite to xVel;
    player.moveBackX();
    quad1.x -= xVel;

    //move back enemies.
    for(int i = 0; i < 20; i++)
    {
      basicEnemy[i].scrollX(player.getXvel());
    }
    //move back bullets.
    for(int i = 0; i < 100; i++)
    {
      bullets[i].scrollX(player.getXvel());
    }
  }

  //do the same on other side of level.
  if(player.getRect().x > SCREEN_WIDTH - player.getRect().w - 200 && quad2.x + quad2.w > SCREEN_WIDTH - 50)
  {
    //move player and map opposite to xVel;
    player.moveBackX();
    quad1.x -= xVel;

    //move back enemies.
    for(int i = 0; i < 20; i++)
    {
      basicEnemy[i].scrollX(player.getXvel());
    }
    //move back bullets.
    for(int i = 0; i < 100; i++)
    {
      bullets[i].scrollX(player.getXvel());
    }
  }

  //now do the same but on y axis.
  if(player.getRect().y < 50 && quad1.y < 50)
  {
    player.moveBackY();
    quad1.y -= yVel;

    for(int i = 0; i < 20; i++)
    {
      basicEnemy[i].scrollY(player.getYvel());
    }

    for(int i = 0; i < 100; i++)
    {
      bullets[i].scrollY(player.getYvel());
    }
  }

  if(player.getRect().y > SCREEN_HEIGHT - player.getRect().h - 50 && quad3.y + quad3.h > SCREEN_HEIGHT - 50)
  {
    player.moveBackY();
    quad1.y -= yVel;

    for(int i = 0; i < 20; i++)
    {
      basicEnemy[i].scrollY(player.getYvel());
    }

    for(int i = 0; i < 100; i++)
    {
      bullets[i].scrollY(player.getYvel());
    }
  }

  if(player.getRect().x < quad1.x || player.getRect().x + player.getRect().w > quad2.x + quad2.w)
  {
    player.moveBackX();
  }
  if(player.getRect().y < quad1.y || player.getRect().y + player.getRect().h > quad3.y + quad3.h)
  {
    player.moveBackY();
  }

  quad2.x = quad1.x + quad1.w;
  quad2.y = quad1.y;

  quad3.x = quad1.x;
  quad3.y = quad1.y + quad1.h;

  quad4.x = quad1.x + quad1.w;
  quad4.y = quad1.y + quad1.h;
}

int ClassicMode::getMouseX()
{
  return mousePosX;
}

int ClassicMode::getMouseY()
{
  return mousePosY;
}

void ClassicMode::setMouse(int x, int y)
{
  mousePosX = x;
  mousePosY = y;
}

void ClassicMode::setMouse(SDL_Window* window)
{
  if(mousePosX > player.getRect().x + (player.getRect().w * 10))
  {
    mousePosX = player.getRect().x + (player.getRect().w * 10);
    SDL_WarpMouseInWindow(window, mousePosX, mousePosY);
  }
  if(mousePosY > player.getRect().y + (player.getRect().h * 10))
  {
    mousePosY = player.getRect().y + (player.getRect().h * 10);
    SDL_WarpMouseInWindow(window, mousePosX, mousePosY);
  }
  if(mousePosX < player.getRect().x - (player.getRect().w * 10))
  {
    mousePosX = player.getRect().x - (player.getRect().w * 10);
    SDL_WarpMouseInWindow(window, mousePosX, mousePosY);
  }
  if(mousePosY < player.getRect().y - (player.getRect().h * 10))
  {
    mousePosY = player.getRect().y - (player.getRect().h * 10);
    SDL_WarpMouseInWindow(window, mousePosX, mousePosY);
  }
  //cursor should also move relative to player but this is broken.
/**  mousePosX += player.getXvel();
  if(player.getRect().x < 200 && quad1.x < 50 ||
      player.getRect().x > SCREEN_WIDTH - player.getRect().w - 200 && quad2.x + quad2.w > SCREEN_WIDTH - 50)
      {
        mousePosX -= player.getXvel();
      }
    mousePosY += player.getYvel();
   if(player.getRect().y < 50 && quad1.y < 50 ||
     player.getRect().y > SCREEN_HEIGHT - player.getRect().h - 50 && quad3.y + quad3.h > SCREEN_HEIGHT - 50)
     {
       mousePosY -= player.getYvel();
     }
   SDL_WarpMouseInWindow(window, mousePosX, mousePosY);**/

}
