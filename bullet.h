#ifndef _Bullet_Class_
#define _Bullet_Class_

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<stdio.h>
#include<string>
#include<stdlib.h>
#include <time.h>

#include"functions.h"
class Bullet {
private:
  SDL_Rect rect;
  double xVel, yVel;
  double xTemp, yTemp;
  double angle;
public:
  void init();
  void spawn(SDL_Rect start, SDL_Rect target);
  void doStuff(SDL_Renderer* renderer, SDL_Rect target, SDL_Texture* bulletImage, SDL_Rect level, bool paused);
  bool active;
  bool getActive();
  SDL_Rect getRect();
  void moveX();
  void moveY();
  void setVel(SDL_Rect target);
  void scrollX(int vel);
  void scrollY(int vel);
};
#endif
