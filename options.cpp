#include"options.h"

void OptionMenu::init(int cursorStyle, bool showFps)
{
  status = 1;

  menuRect.x = 400;
  menuRect.y = 300;
  menuRect.w = 200;
  menuRect.h = 100;

  option1.x = 460;
  option1.y = 320;
  option1.w = 70;
  option1.h = 20;

  option1Checker.x = 540;
  option1Checker.y = 320;
  option1Checker.w = 50;
  option1Checker.h = 20;

  option2.x = 460;
  option2.y = 345;
  option2.w = 70;
  option2.h = 20;

  option2Checker.x = 540;
  option2Checker.y = 345;
  option2Checker.w = 50;
  option2Checker.h = 20;

  optionQuit.x = 460;
  optionQuit.y = 365;
  optionQuit.w = 40;
  optionQuit.h = 20;

  fpsRect.x = 700;
  fpsRect.y = 30;
  fpsRect.w = 0;
  fpsRect.h = 0;

  cursor.x = 0;
  cursor.y = 0;
  cursor.w = 10;
  cursor.h = 10;

  mousePosX = mousePosY = 0;

  quitMenu = false;
  keyEngine.setKeys();

  black = {0, 0, 0};
  green = {30, 255, 0};

  cursorSetter = cursorStyle;
  fpsSetter = showFps;
}

void OptionMenu::doMenu(
  SDL_Renderer* renderer,
  SDL_Texture* gameBackGround,
  TTF_Font* gameFont,
  float avgFPS,
  bool showFps,
  SDL_Texture* cursor1,
  SDL_Texture* cursor2,
  SDL_Texture* cursorDuck,
  SDL_Texture* cursorWatcher,
  SDL_Texture* cursorCrossHair,
  int cursorStyle )
{
  //Mouse events
  while(SDL_PollEvent(&eventHandler) != 0)
  {
    if(eventHandler.type == SDL_QUIT)
    {
      quitMenu = true;
    }
    //mouse events
    if(eventHandler.type == SDL_MOUSEMOTION || eventHandler.type == SDL_MOUSEBUTTONDOWN || eventHandler.type == SDL_MOUSEBUTTONUP )
    {//update the mouse position
      SDL_GetMouseState(&mousePosX, &mousePosY );
      //check if buttons were pressed or released
      switch (eventHandler.type) {
        case SDL_MOUSEBUTTONUP:
          if(eventHandler.button.button == SDL_BUTTON_LEFT){
            printf("left button released\n");
            if(checkCollision(cursor, option1Checker))
            {
              cursorSetter++;
              if(cursorSetter > 5)
              {
                cursorSetter = 1;
              }
            }
            else if(checkCollision(cursor, option2Checker))
            {
              if(fpsSetter)
              {
                fpsSetter = false;
              }
              else
              {
                fpsSetter = true;
              }
            }
            else if(checkCollision(cursor,optionQuit)){
              status = 1;
            }
          }
          else if(eventHandler.button.button == SDL_BUTTON_RIGHT){
            printf("Right button released\n");
          }
          break;
        case SDL_MOUSEBUTTONDOWN:
          if(eventHandler.button.button == SDL_BUTTON_LEFT){
            printf("Left button pressed\n");
          }
          else if(eventHandler.button.button == SDL_BUTTON_RIGHT){
            printf("Right button pressed\n");
          }
          break;
      }
      printf("Mouse x pos:");
      printf("%d\n",mousePosX);
      printf("Mouse y pos:");
      printf("%d\n",mousePosY);
    }
  }
  //update the cursor position
  cursor.x = mousePosX;
  cursor.y = mousePosY;
  //update the keys and assign them to local variables.
  keyEngine.updateKeys();
  for(int i = 0; i < 4; i++)
  {
    gameKeys[i] = keyEngine.getKeys(i);
  }
  //set the draw color and clear the screen
  SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
  SDL_RenderClear(renderer);

  SDL_RenderCopy(renderer, gameBackGround, NULL, NULL);

  //draw a menu box
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 1);
  SDL_RenderFillRect(renderer, &menuRect);
  SDL_SetRenderDrawColor(renderer, 30, 255, 0, 1);
  SDL_RenderDrawRect(renderer, &menuRect);

  //draw the options to the Screen
  renderText(renderer, "Cursor", gameFont, green, option1.x, option1.y);
  renderText(renderer, "Next", gameFont, green, option1Checker.x, option1Checker.y);
  renderText(renderer, "Show FPS", gameFont, green, option2.x, option2.y);
  if(showFps)
    renderText(renderer, "Yes", gameFont, green, option2Checker.x, option2Checker.y);
  else
    renderText(renderer, "No", gameFont, green, option2Checker.x, option2Checker.y);
  renderText(renderer, "Return", gameFont, green, optionQuit.x, optionQuit.y);

  if(showFps)
  {
    fpsText.str( "" );
    fpsText << "Average Frames Per Second " << avgFPS;
    renderText(renderer, fpsText.str().c_str(), gameFont, green, fpsRect.x, fpsRect.y);
  }

  //draw the cursor
  switch (cursorStyle) {
    case 1:
      drawCursor(renderer, cursor, cursor, cursor1); //supply cursor as origin to stop rotation.
      break;
    case 2:
      drawCursor(renderer, cursor, option1, cursor2); //supply player rect to rotate cusor
      break;
    case 3:
      drawCursor(renderer, cursor, cursor, cursorDuck);
      break;
    case 4:
      drawCursor(renderer, cursor, option1, cursorWatcher);
      break;
    case 5:
      drawCursor(renderer, cursor, cursor, cursorCrossHair);
      break;
  }
  //update the Screen
  SDL_RenderPresent(renderer);
}

bool OptionMenu::getMenuStatus()
{
  return quitMenu;
}

int OptionMenu::getStatus()
{
  return status;
}

void OptionMenu::setStatus(int status)
{
  this->status = status;
}

int OptionMenu::getMouseX()
{
  return mousePosX;
}

int OptionMenu::getMouseY()
{
  return mousePosY;
}

void OptionMenu::setMouse(int x, int y)
{
  mousePosX = x;
  mousePosY = y;
}

int OptionMenu::getCursor()
{
  return cursorSetter;
}

bool OptionMenu::getFps()
{
  return fpsSetter;
}
