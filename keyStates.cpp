#include"keyStates.h"

//w = 0
//A = 1
//S = 2
//D = 3
//ESC = 4

void keyChecker::setKeys()
{
  for(int i = 0; i < 5; i++)
  {
    keys[i] = false;
  }
}

void keyChecker::updateKeys()
{
  const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );
  if(currentKeyStates[SDL_SCANCODE_W])
  {
    keys[0] = true;
  }
  else
  {
    keys[0] = false;
  }
  if(currentKeyStates[SDL_SCANCODE_A])
  {
    keys[1] = true;
  }
  else
  {
    keys[1] = false;
  }
  if(currentKeyStates[SDL_SCANCODE_S])
  {
    keys[2] = true;
  }
  else
  {
    keys[2] = false;
  }
  if(currentKeyStates[SDL_SCANCODE_D])
  {
    keys[3] = true;
  }
  else
  {
    keys[3] = false;
  }
  if(currentKeyStates[SDL_SCANCODE_ESCAPE])
  {
    keys[4] = true;
  }
  else
  {
    keys[4] = false;
  }
}

bool keyChecker::getKeys(int index)
{
  return keys[index];
}
