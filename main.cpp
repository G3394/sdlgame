#include"game.h"

int main( int argc, char* args[] )
{
  //instantiate the game object
  Game Game;
  Game.init();
  Game.gameLoad();

  Game.doGame();
  Game.quit();
  return 0;
}
