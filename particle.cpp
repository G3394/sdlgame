#include"particle.h"

void Particle::init()
{
  rect.x = rand() % 500;
  rect.y = rand() % 500;
  rect.w = 20;
  rect.h = 20;

  xTemp = yTemp = 0;

  xVel = yVel = 0;

  active = false;
  angle = 0.0;

  lifeSpan = 0;
}

void Particle::spawn(SDL_Rect start, SDL_Rect target, int speed)
{
  xTemp = start.x;
  yTemp = start.y;
  rect.x = xTemp;
  rect.y = yTemp;

  setVel(target, speed);
  active = true;

  lifeSpan = rand() % 30;
}

void Particle::doStuff(SDL_Renderer* renderer, SDL_Rect start, bool paused)
{
  if(active)
  {
    if(!paused)
    {
      moveX();
      moveY();
      lifeSpan--;
    }
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 0);
    SDL_RenderDrawLine(renderer, rect.x - 2, rect.y, rect.x + 2, rect.y);
    SDL_RenderDrawLine(renderer, rect.x, rect.y - 2, rect.x, rect.y + 2);
  }
  else
  {
    xTemp = start.x;
    yTemp = start.y;
  }

  if(lifeSpan < 0)
  {
    active = false;
  }

}

SDL_Rect Particle::getRect()
{
  return rect;
}

bool Particle::getActive()
{
  return active;
}

void Particle::moveX()
{
  xTemp += xVel;
  rect.x = xTemp;

}

void Particle::moveY()
{
  yTemp += yVel;
  rect.y = yTemp;
}

void Particle::setVel(SDL_Rect target, int speed)
{
  double tx = target.x - rect.x;
  double ty = target.y - rect.y;

  double pi = 3.14;

  angle = atan2(ty, tx) * 180 / pi;

  xVel = speed*(cos(angle*pi/180));   // move x
  yVel = speed*(sin(angle*pi/180));   // move x
}

void Particle::scrollX(int vel)
{
  xTemp -= vel;
  rect.x = xTemp;
}

void Particle::scrollY(int vel)
{
  yTemp -= vel;
  rect.y = yTemp;
}
