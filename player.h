#ifndef _Player_Class_
#define _Player_Class_

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<stdio.h>
#include<string>
#include<stdlib.h>
#include <time.h>

#include"functions.h"
class Player {
private:
  SDL_Rect rect;
  double xVel, yVel;
  double angle;
public:
  void init(int SCREEN_WIDTH, int SCREEN_HEIGHT);
  void doStuff(SDL_Renderer* renderer, SDL_Texture* playerImage, bool paused);
  SDL_Rect getRect();
  int getXvel();
  int getYvel();
  void setXvel(int vel);
  void setYvel(int vel);

  void moveBackX();
  void moveBackY();
};
#endif
